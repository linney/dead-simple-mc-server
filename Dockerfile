FROM openjdk:18

COPY entrypoint.sh /

VOLUME /data

CMD sh entrypoint.sh
